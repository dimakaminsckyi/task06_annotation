package com.epam.view;

import com.epam.controller.ReflectionController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private Map<String, Runnable> commandMap;
    private Map<String, String> menu;
    private ReflectionController reflectionController;
    private static Logger log = LogManager.getLogger(MainView.class);

    public MainView() {
        reflectionController = new ReflectionController();
        initMenu();
        commandMap = new HashMap<>();
        commandMap.put("menu", this::showMenu);
        commandMap.put("field", () -> reflectionController.showAnnotationField());
        commandMap.put("invoke", () -> reflectionController.invokeThreePrivateMethods());
        commandMap.put("invokeArgs", () -> reflectionController.invokeMethodsWithArrayArg());
        commandMap.put("setField", () -> reflectionController.setValue("John"));
        commandMap.put("info", () -> reflectionController.showInfo());
        commandMap.put("exit", () -> log.info("Bye"));
    }

    public void show() {
        Scanner scanner = new Scanner(System.in);
        String flagExit;
        log.info("input 'menu' to show Menu");
        do {
            flagExit = scanner.next();
            try {
                commandMap.get(flagExit).run();
            } catch (NullPointerException e) {
                log.info("Input correct option\n");
            }
        } while (!flagExit.equals("exit"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("field", "Show Annotation field");
        menu.put("invoke", "Invoke 3 private methods from Student");
        menu.put("info", "Show info class");
        menu.put("invokeArgs", "Invoke methods whith array args");
        menu.put("setField", "Set field firstName");
        menu.put("exit", "Exit");
    }

    private void showMenu() {
        menu.values().forEach(s -> log.info(s));
        log.info("Inputs: ");
        menu.keySet().forEach(k -> System.out.print(k + " "));
    }
}
