package com.epam.controller;

import com.epam.model.InfoClass;
import com.epam.model.InfoStudent;
import com.epam.model.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionController {

    private static Logger log = LogManager.getLogger(ReflectionController.class);
    private Class clazz;
    private Student student;

    public ReflectionController() {
        student = new Student();
        clazz = student.getClass();
    }

    public void showAnnotationField() {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(InfoStudent.class)) {
                InfoStudent annotation = field.getAnnotation(InfoStudent.class);
                if (field.getName().equals("firstName")) {
                    printField(field.getName(), annotation.firstName());
                } else if (field.getName().equals("lastName")) {
                    printField(field.getName(), annotation.lastName());
                } else {
                    printField(field.getName(), String.valueOf(annotation.age()));
                }
            }
        }
    }

    public void invokeThreePrivateMethods() {
        try {
            invokeCountStipendPerMonth();
            invokeCountStipendPerMonthDouble();
            invokeshowStipendPerMonth();
        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void setValue(String value) {
        try {
            Field field = clazz.getDeclaredField("firstName");
            field.setAccessible(true);
            field.set(student, value);
            log.info(field.get(student));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

    public void showInfo() {
        InfoClass infoClass = new InfoClass(student);
        infoClass.showInfo();
    }

    public void invokeMethodsWithArrayArg() {
        String[] wordArray = {"A", "B", "C"};
        int[] numberArray = {5, 4, 3, 2, 1};
        try {
            Method method = clazz.getDeclaredMethod("showWords", String[].class);
            method.setAccessible(true);
            log.info(method.invoke(student, (Object) wordArray));
            Method secondMethod = clazz.getDeclaredMethod("countNumbers", String.class, int[].class);
            secondMethod.setAccessible(true);
            log.info(secondMethod.invoke(student, "Number", numberArray));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private void invokeCountStipendPerMonth() throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        Method method = clazz.getDeclaredMethod("countStipendPerMonth");
        method.setAccessible(true);
        log.info(method.invoke(student));
    }

    private void invokeCountStipendPerMonthDouble() throws NoSuchMethodException, NoSuchFieldException,
            IllegalAccessException, InvocationTargetException {
        Field field = clazz.getDeclaredField("stipendPerDay");
        field.setAccessible(true);
        Method method = clazz.getDeclaredMethod("countStipendPerMonthDouble", int.class);
        method.setAccessible(true);
        log.info(method.invoke(student, field.get(student)));
    }

    private void invokeshowStipendPerMonth() throws InvocationTargetException, IllegalAccessException,
            NoSuchMethodException {
        Method method = clazz.getDeclaredMethod("showStipendPerMonth");
        method.setAccessible(true);
        log.info(method.invoke(student));
    }

    private void printField(String nameField, String value) {
        log.info("@InfoStudent(" + nameField + " = " + value + ")");
    }
}
