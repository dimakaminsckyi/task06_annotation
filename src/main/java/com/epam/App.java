package com.epam;

import com.epam.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().show();
    }
}
