package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class InfoClass {

    private static Logger log = LogManager.getLogger(InfoClass.class);
    private Class clazz;

    public InfoClass(Object object) {
        clazz = object.getClass();
    }

    public void showInfo() {
        log.info("Class name : " + clazz.getSimpleName());
        for (Field field : clazz.getDeclaredFields()) {
            log.info("Field : " + field.getType() + " " + field.getName());
        }
        for (Method method : clazz.getDeclaredMethods()) {
            log.info("Method : " + method.getReturnType() + " " + method.getName());
        }
        for (Constructor constructor : clazz.getDeclaredConstructors()) {
            log.info("Constructor : " + constructor.getName() + " " +
                    Arrays.toString(constructor.getParameterTypes()));
        }
    }
}
