package com.epam.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface InfoStudent {

    String firstName() default "John";

    String lastName() default "Doe";

    int age() default 20;
}
